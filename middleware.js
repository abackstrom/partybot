const axios   = require('axios');
const cheerio = require('cheerio');
const Discord = require('discord.js');

// This really just awaits any promise, but in practice we intend
// to use this for the Client.login() promise.
exports.requireLogin = function(promise) {
  return async (request, response, next) => {
    await promise;
    next();
  };
};

// Ensure the request has specified a specific secret token in the query
// string's "token" parameter.
exports.requireToken = function(token) {
  return async (request, response, next) => {
    if (request.query.token === token) {
      return next();
    }

    console.log("Client supplied wrong web token: " + request.query.token);
    response.status(403).send("403 Forbidden");
  };
};

exports.handleMessage = function(message) {
    var client = this;

    if (message.cleanContent.substr(0, 1) !== "!") {
        return check_mentions(this, message);
    }

    var args = message.cleanContent.trimRight().split(/\s+/);
    var cmd = args.shift();

    console.log(cmd, args);

    // catch empty command
    if (cmd === "!") {
        return;
    }

    cmd = cmd.substr(1);

    if (cmd === "summon") {
        return cmd_summon(this, message, args);
    } else if (cmd === "gif") {
        return cmd_gif(this, message, args);
    }
};

function check_mentions(client, message) {
    const reactions = ["😉", "🙃", "😘", "🤪",  "😏", "😝"];
    if (message.mentions.users.get(client.user.id)) {
        message.react(reactions[Math.floor(Math.random() * reactions.length)]);
    }
}

function sendImageAsRichEmbed(channel, url) {
    channel.send(new Discord.RichEmbed()
        .setImage(url)
        .setColor('BLUE')
    ).catch(console.error);
}

function cmd_summon(client, message, args) {
    var search = args.join(" ");
    var url = "https://api.bing.microsoft.com/v7.0/images/search?q=" + encodeURIComponent(search);
    var channel = client.channels.get(message.channel.id);

    channel.startTyping();

    axios({
        baseURL: url,
        headers: {
            'Ocp-Apim-Subscription-Key': process.env.AZURE_KEY
        },
    }).then(function(json) {
        sendImageAsRichEmbed(channel, json.data.value[0].thumbnailUrl);
        channel.stopTyping();
    }).catch(function(err) {
        console.error(err);
        channel.stopTyping();
    });
};

function cmd_gif(client, message, args) {
    var channel = client.channels.get(message.channel.id);
    channel.startTyping();

    var search = args.join(" ");
    axios({
        baseURL: "https://api.tenor.com/v1/search",
        params: {
            key: process.env.TENOR_KEY,
            q: search,
            limit: 1,
            media_filter: "minimal"
        },
    })
        .then(json => {
            var result = json.data.results.shift();
            if (!result) {
                throw "No results";
            }

            var gif = result.media[0].gif;
            sendImageAsRichEmbed(channel, gif.url);

            channel.stopTyping();
        })
        .catch(function(err) {
            console.error(err);
            channel.stopTyping();
        });
}
