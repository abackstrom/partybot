require('dotenv').load();

// init project
const express = require('express');
var app = express();
var partybot = require('./middleware');

const pug = require('pug');
app.set('view engine', 'pug');

const sassMiddleware = require("node-sass-middleware");
app.use(sassMiddleware({
  src: __dirname + '/static',
  dest: '/tmp'
}));

require('express-async-errors');

var Discord = require('discord.js');
var client = new Discord.Client();
var login_promise = client.login(process.env.BOT_TOKEN);

client.on('ready', () => {
    console.log("partybot is now ready");
});

client.on('error', error => {
    console.log(error);
});

client.on('message', partybot.handleMessage.bind(client));

app.use(express.static('static'));
app.use(express.static('/tmp'));

app.use('/api', partybot.requireLogin(login_promise));
app.use('/api', partybot.requireToken(process.env.WEB_TOKEN));

app.get("/", function (request, response) {
  response.render('index', {
    client_id: process.env.CLIENT_ID
  });
});

app.get("/api/get-messages/:channel_id", async (request, response) => {
  var messages = await client.channels.get(request.params.channel_id).fetchMessages();

  // Delete old chat messages from our bot:
  messages.filter(m => m.author.tag === process.env.BOT_TAG && m.type === 'DEFAULT').map(m => m.delete());

  // Send a message to a channel:
  client.channels.get(request.params.channel_id).send("I'm alive! And I only say one thing! " + Date.now());

  // Get some messages from the channel:
  var messages = await client.channels.get(request.params.channel_id).fetchMessages();
  response.send(messages.filter(m => m.type === 'DEFAULT').map(m => m.toString()));
});

var listener = app.listen(process.env.BOT_PORT || 8080, process.env.BOT_HOST || '0.0.0.0', function () {
  console.log('Your app is listening on port ' + listener.address().port);
});

